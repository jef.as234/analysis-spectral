AHOCODER - README
=================

OUTLINE
-------

1. INTRODUCTION
2. USAGE
   2.1. AHOCODER16
   2.2. AHODECODER16
3. INFORMATION FOR HTS USERS
4. RELATED PUBLICATIONS
5. AUTHORS
6. ACKNOWLEDGMENTS


1. INTRODUCTION
---------------

AHOcoder is a speech vocoder developed by Daniel Erro at AHOLAB Signal
Processing Laboratory (http://aholab.ehu.es), University of the Basque
Country (UPV/EHU), Bilbao, Spain.

AHOcoder parameterizes speech waveforms (in wav-mono-16kHz-16bits format)
into three different streams: pitch (logf0), spectrum (Mel-cepstral coeffs.),
and optionally excitation (maximum voiced frequency).

AHOcoder is reported to provide accurate analysis and high quality
waveform reconstruction. It is a very suitable for statistical speech
processing tasks such as HMM-based speech synthesis and GMM-based voice
conversion. Moreover, it can be used just for basic speech manipulation
and transformation (pitch level and variance, speaking rate, vocal tract
length...).

This distribution of AHOcoder contains two executable binary files built
using gcc 4.7 under linux (64bits):
- ahocoder16_64 translates waveforms into parameters.
- ahodecoder16_64 translates parameters into synthetic waveforms.

AHOcoder is still an experimental tool. Please contact us if you find any
problem. Any suggestion will be highly appreciated.


2. USAGE
--------

2.1. AHOCODER16

The basic call to AHOcoder has the following form:

  ahocoder16_64 filein filef0 filecc filefv [params]

where filein represents the input wav file (mono 16kHz 16bits), filef0
is the output file containing the measured logf0 values, filecc is the
output file containing the Mel-cepstral coefficients, and filefv is an
optional output file containing the maximum voiced frequency.

There is a set of optional parameters adjustable by the user. The
parameters must be specified after the mandatory arguments under the 
form --name=value. In this version of the tool, the following parameters
are available (their default values are also indicated):

   --lframe=80     Frame shift (samples)
   --ccord=39      Order of the cepstral representation
   --ccmeth=1      0: harmonics + interpolation + mcep
                   1: harmonics + mel-rdc
                   2: quasi-harmonics + mel-rdc
                   3: complementary windows + liftering + mcep
                   4: seevoc + mcep
   --f0min=60      Lowest detectable pitch (Hz)
   --f0max=500     Highest detectable pitch (Hz)
   --f0acc=0       f0 refinement iterations using qhm [0...2]
   --f0load=0      Load pitch file if it already exists


2.2. AHODECODER16

The basic call to AHOdecoder has the following form:

  ahodecoder16_64 filef0 filecc filefv fileout [params]
 
where filef0 is the input file containing the logf0 values, filecc is
the input file containing the cepstral coefficients, filefv is an
optional input file containing the maximum voiced frequency, and fileout
is the output wav file (mono 16kHz 16bits).
 
There is a set of optional parameters adjustable by the user. The
parameters must be specified after the mandatory arguments under the 
form --name=value. In this version of the tool, the following parameters
are available (their default values are also indicated):

   --lframe=80     Frame shift (samples)
   --fpitch=1.0    Pitch modification factor [0.25...4]
   --fvpitch=1.0   Log-pitch variance modification factor [0...4]
   --fdur=1.0      Duration modification factor [0.25...4]
   --fwarp=0.0     All-pass factor for vocal tract modification [-1...1]
   --floud=0.0     Factor for perceptual loudness [-4...4]
   --fpfilt=0.0    Postfiltering factor [0...1]
   --wavnorm=0     Adapt wav amplitude to available range


3. INFORMATION FOR HTS USERS
----------------------------

AHOcoder is reported to be an ideal complement for HTS. The output files
generated by AHOcoder contain float numbers without header, so they are
fully compatible with the HTS demo scripts in the HTS website. You can use
the same configuration as in the STRAIGHT-based demo, using the bap stream
to handle the excitation (maximum voiced frequency). Just place the output
files generated by AHOcoder in the suitable data subfolders (lf0, mgc, and
bap, respectively) and replace the dimension of the bap stream by 1 in
both the cmp generation and the configuration+training scritps.


4. RELATED PUBLICATIONS
-----------------------

The technical details of AHOcoder have been published mainly in the
following papers:

- D. Erro, I. Sainz, E. Navas, I. Hernaez,
  "Harmonics plus Noise Model based Vocoder for
  Statistical Parametric Speech Synthesis",
  IEEE Journal of Selected Topics in Signal Processing, 2013.

- D. Erro, I. Sainz, E. Navas, I. Hernaez,
  "Efficient Spectral Envelope Estimation from Harmonic Speech Signals",
  IET Electronics Letters, vol. 48(16), pp. 1019-1021, 2012.

- D.Erro, I.Sainz, E.Navas, I.Hernaez,
  "Improved HNM-based Vocoder for Statistical Synthesizers",
  InterSpeech, pp. 1809-1812, Florence, August 2011.

- D.Erro, I.Sainz, E.Navas, I.Hernaez,
  "HNM-based MFCC+F0 Extraxtor applied to Statistical Speech Synthesis",
  IEEE International Conference on Acoustics, Speech and Signal Processing
  (ICASSP), pp. 4728-4731, Prague, May 2011.

- D.Erro, I.Sainz, I.Saratxaga, E.Navas, I.Hernaez,
  "MFCC+F0 extraction and waveform reconstruction using HNM:
  preliminary results in an HMM-based synthesizer",
  VI Jornadas en Tecnologia del Habla & II Iberian SLTech (FALA),
  pp. 29-32, Vigo, November 2010.


5. AUTHORS
----------

Contact author:
Daniel Erro (Ikerbasque Research Fellow), derro@aholab.ehu.es

Contributors:
Iñaki Sainz (speech synthesis expert), inaki@aholab.ehu.es
Eva Navas (leader of related research projects), eva@aholab.ehu.es
Inma Hernaez (head of AHOLAB), inma@aholab.ehu.es


6. ACKNOWLEDGMENTS
------------------

This work was partially funded by UPV/EHU (Ayuda de Especialización de
Doctores), the Spanish Ministry of Science and Innovation (Buceador,
TEC2009-14094-C04-02), the Spanish Ministry of Economy and Competitiveness
(SpeechTech4All, TEC2012-38939-C03-03), and the Basque Government
(Berbatek, IE09-262; Ber2tek, IE12-333).
