#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import easygui as eg
import easygui
import sys
import os
import subprocess

from pylatex import Document, Section, Subsection, Tabular, Math, TikZ, Axis, \
    Plot, Figure, Matrix, Alignat
from pylatex.utils import italic
import webbrowser

import threading
from numpy import zeros
import glob #directorios

def vaciarDir(dir,end): #ELimina archivos de un directorio
	filelist = [ f for f in os.listdir(dir) if f.endswith(end) ]
	for f in filelist:
		os.remove(dir+"/"+f)


def msgEspera(choices,msg): #Msj de espera
	title='Análisis espectral'
	eleccion=easygui.buttonbox(msg, title, choices)
	if eleccion == choices[0]:
		print 'OK'
	else:
		sys.exit()

def msgFin(choices,msg): #Msj de análisis finalizado
	title='Análisis espectral'
	eleccion=easygui.buttonbox(msg, title, choices)
	if eleccion == choices[0]:
		sys.exit()
	elif eleccion==choices[1]:
		os.system("evince ../out/pdf/informe.pdf")
		#os.system("sh ../script/latex.sh")
		sys.exit()
	else:
			os.system("evince ../out/pdf/informeResumen.pdf")
			sys.exit()

def resumen(Nruta1,flagDE,flagDAM,flagDM): #Crea informe resumen
	archivoR=open('../out/pdf/informeResumen.tex', 'w')
	archivoR.write('\\documentclass{article}\n\\usepackage[utf8]{inputenc}\n\\usepackage{graphicx}\n\\usepackage{float}\n\\title{Informe resumen: Sistema de análisis espectral entre conjuntos de señales de habla: Métrica Distancia Euclídea, Distancia absoluta media (MAD) y Distancia de Manhattan}\n\\author{EIE}\n\\begin{document}\n\\maketitle\n')
	
	if flagDE==1: #Resumen Euclidea
		archivoR.write('\\newpage \n')
		archivoR.write('\\section{Distancia Euclídea} \n')
		#Inicio de tabla para latex
		archivoR.write('\\begin{table}[H] \n')
		archivoR.write('\\centering \n')
		archivoR.write('\\caption{Distancia euclídea}')
		archivoR.write('\\begin{tabular}{|l|l|l|l|}\n')
		archivoR.write('\\hline \n')
		archivoR.write('MFCC & Distancia euclídea & MFCC & Distancia euclídea \\\\')
		archivoR.write('\\hline \n')
		###########
		global array_ponderado_DE
		array_ponderado_DE=array_ponderado_DE/Nruta1
		array_ponderado_DE=np.array(array_ponderado_DE).tolist()
		for conteo in range(len(array_ponderado_DE)-20): #ciclo for para imprimir la lista de distancias ponderada
			conteo2=str(conteo+1)
			conteolado2=str(conteo+21)
			y=str(array_ponderado_DE[conteo+1])
			if conteo == 19:
				y2='-'
			else:
				y2=str(array_ponderado_DE[conteo+21])
			archivoR.write('MFCC ' + conteo2 + '&' + y + '&' 'MFCC ' + conteolado2 + '&' + y2)
			archivoR.write('\\\\ \\hline\n')
		archivoR.write('\\end{tabular} \n')
		archivoR.write('\\end{table} \n')
		#Barra grafico
		array_ponderado_DE.pop(0)# ELiminar coeficiente de energia
		Cepstros=np.arange(len(array_ponderado_DE))
		plt.bar(Cepstros, array_ponderado_DE)
		plt.title('Distancia Euclideana')
		plt.xlabel('Coeficientes Cepstrales')
		plt.ylabel('Distancia Euclideana')	
		plt.savefig('../out/pictures/DE.png')
		archivoR.write('\n \\begin{figure}[H]\n \\centering\n \\includegraphics[width=1\\textwidth]{' + '../out/pictures/DE.png' + '}\n')
		archivoR.write('\\caption{Gráfico de barras para distancia euclídea}\n \\label{fig:barraDM}\n \\end{figure}')
		plt.close()

	if flagDAM==1: #MAD resumen
		global ponderado_DAM 
		global array_ponderado_DAM
		array_ponderado_DAM=array_ponderado_DAM/Nruta1
		ponderado_DAM=ponderado_DAM/Nruta1
		array_ponderado_DAM=np.array(array_ponderado_DAM).tolist()
		#latex 
		archivoR.write('\\newpage \n')
		archivoR.write('\\section{MAD}' + '\n\n')
		archivoR.write('\\textbf{Los resultados para la métrica MAD son: } ' + '\n\n')
		archivoR.write('\nMAD: ' + str(ponderado_DAM) + '\n\n')
		archivoR.write('\\textbf{La distancia media absoluta es:} ' + '\n\n ')
		#Inicio de tabla para latex
		archivoR.write('\\begin{table}[H] \n')
		archivoR.write('\\centering \n')
		archivoR.write('\\caption{Distancia media absoluta}')
		archivoR.write('\\begin{tabular}{|l|l|l|l|}\n')
		archivoR.write('\\hline \n')
		archivoR.write('MFCC & MAD & MFCC & MAD \\\\')
		archivoR.write('\\hline \n')
		###########
		for conteo in range(len(array_ponderado_DAM)-19): #ciclo for para imprimir la lista de MAD ponderada
			conteo2=str(conteo+1)
			conteolado2=str(conteo+21)
			y=str(array_ponderado_DAM[conteo])
			if conteo == 19:
				y2='-'
			else:
				y2=str(array_ponderado_DAM[conteo+20])
			archivoR.write('MFCC ' + conteo2 + '&' + y + '&' 'MFCC ' + conteolado2 + '&' + y2)
			archivoR.write('\\\\ \\hline\n')
		archivoR.write('\\end{tabular} \n')
		archivoR.write('\\end{table} \n')
		#Barra grafico
		Cepstros=np.arange(len(array_ponderado_DAM))
		plt.bar(Cepstros, array_ponderado_DAM)
		plt.title('Distancia absoluta media')
		plt.xlabel('Coeficientes Cepstrales')
		plt.ylabel('MAD')	
		plt.savefig('../out/pictures/DAM.png')
		archivoR.write('\n \\begin{figure}[H]\n \\centering\n \\includegraphics[width=1\\textwidth]{' + '../out/pictures/DAM.png' + '}\n')
		archivoR.write('\\caption{Gráfico de barras para Distancia absoluta media}\n \\label{fig:barraDaM}\n \\end{figure}')
		plt.close()

	if flagDM==1: # Manhattan ponderado
		global array_ponderado_DM 
		array_ponderado_DM=array_ponderado_DM/Nruta1
		array_ponderado_DM=np.array(array_ponderado_DM).tolist()
		#latex
		archivoR.write('\\newpage \n')
		archivoR.write('\\section{Distancia de Manhattan}' + '\n\n')
		#Inicio de tabla para latex
		archivoR.write('\\begin{table}[H] \n')
		archivoR.write('\\centering \n')
		archivoR.write('\\caption{Distancia de Manhattan}')
		archivoR.write('\\begin{tabular}{|l|l|l|l|}\n')
		archivoR.write('\\hline \n')
		archivoR.write('MFCC & Manhattan & MFCC & Manhattan \\\\')
		archivoR.write('\\hline \n')
		###########
		for conteo in range(len(array_ponderado_DM)-19): #ciclo for para imprimir la lista de dMAnhattan ponderada
			conteo2=str(conteo+1)
			conteolado2=str(conteo+21)
			y=str(array_ponderado_DM[conteo])
			if conteo == 19:
				y2='-'
			else:
				y2=str(array_ponderado_DM[conteo+20])
			archivoR.write('MFCC ' + conteo2 + '&' + y + '&' 'MFCC ' + conteolado2 + '&' + y2)
			archivoR.write('\\\\ \\hline\n')
		archivoR.write('\\end{tabular} \n')
		archivoR.write('\\end{table} \n')
		#Barra grafico
		Cepstros=np.arange(len(array_ponderado_DM))
		plt.bar(Cepstros, array_ponderado_DM)
		plt.title('Distancia de Manhattan')
		plt.xlabel('Coeficientes Cepstrales')
		plt.ylabel('Manhattan')	
		plt.savefig('../out/pictures/DM.png')
		archivoR.write('\n \\begin{figure}[H]\n \\centering\n \\includegraphics[width=1\\textwidth]{' + '../out/pictures/DM.png' + '}\n')
		archivoR.write('\\caption{Gráfico de barras para Distancia de Manhattan}\n \\label{fig:barraDM}\n \\end{figure}')
		plt.close()
	archivoR.write('\n El promedio se realizó para una cantidad de '+str(Nruta1)+' audios')
	archivoR.write('\n\\end{document}')
	archivoR.close()
	os.system("sh ../script/latexR.sh") # llama script de ltex.sh

def distanceEuclidean(): # Calcula las distncias eucliddeanas y se escribe en el informe total
	global array_ponderado_DE
	# para leer cada archivo de la ruta
	# archivo es la ruta de cada auido
	# archivoRuido es la ruta de cada audio con ruido
	#def devolverArchivos(carpeta):
	rutacsv="../out/mfccAhocoder"
	rutacsvRuido="../out/mfccAhocoderRuido"
	archivoRuido = sorted(os.listdir(rutacsvRuido))
	ind=0

	for archivoaudio in sorted(os.listdir(rutacsv)): #selecciona los archivos a comparar alfabeticamente
		#print(os.path.join(rutacsv,archivoaudio))
		rutaA=os.path.join(rutacsv,archivoaudio)
		archivoruidoRuta=archivoRuido[ind]
		#print(os.path.join(rutacsvRuido,archivoruidoRuta))
		rutaB=os.path.join(rutacsvRuido,archivoruidoRuta)
		ind=ind+1
		Alatex=rutaA.replace("_", "\\_")
		Blatex=rutaB.replace("_", "\\_")
		AlatexARCHI=archivoaudio.replace("_", "\\_")	
		BlatexARCHI=archivoruidoRuta.replace("_", "\\_")		
		csv=pd.read_csv(rutaA, sep='\t', header= None) #leer el archivo
		csvRuido=pd.read_csv(rutaB, sep='\t', header= None)

		#Para archivo de latex#
		archivo.write('\\newpage')
		archivo.write('\\subsection{Métrica Distancia Euclídea para MFCC '+str(ind)+'}' + '\n\n')
		archivo.write('\\textbf{Los archivos analizados son:} \n\n')
		archivo.write('\\begin{enumerate} \n' + '\\item ' + AlatexARCHI + '\n')
		archivo.write('\\item ' + BlatexARCHI + '\n' + '\\end{enumerate} \n\n')
		archivo.write('\\textbf{Las distancias para cada MFCC son: }' + '\n\n')
		######################

		distancias=[] #lista para distancias

		for coeficiente in range(0, 40):	
			CoeficienteActual1=csv.iloc[:,coeficiente].values #datos de una matriz con un solo elemento 1*1 lista de un coefciente
			CoeficienteActual2=csvRuido.iloc[:,coeficiente].values #lista de un coeficeinte con ruido

			d=np.linalg.norm(CoeficienteActual1-CoeficienteActual2) #Función que calcula ladistancia Euclídea
			distancias.append(d) #Se agrega cada distancia a la lista

		myarray = np.asarray(distancias)#convierte lista en array
		#Suma de arrays
		array_ponderado_DE=myarray+array_ponderado_DE

		#Inicio de tabla para latex
		archivo.write('\\begin{table}[H] \n')
		archivo.write('\\centering \n')
		archivo.write('\\caption{Distancia euclidiana para MFCC:'+str(ind)+'}')
		archivo.write('\\begin{tabular}{|l|l|l|l|}\n')
		archivo.write('\\hline \n')
		archivo.write('MFCC & Distancia euclídea & MFCC & Distancia euclídea \\\\')
		archivo.write('\\hline \n')
		###########

		for conteo in range(len(distancias)-20): #ciclo for para imprimir la lista de distancias
			conteo2=str(conteo+1)
			conteolado2=str(conteo+21)
			y=str(distancias[conteo+1])
			if conteo == 19:
				y2='-'
			else:
				y2=str(distancias[conteo+21])
			archivo.write('MFCC ' + conteo2 + '&' + y + '&' 'MFCC ' + conteolado2 + '&' + y2)
			archivo.write('\\\\ \\hline\n')
		archivo.write('\\end{tabular} \n')
		archivo.write('\\end{table} \n')

		#print(' ')
		#print'Con un número de muestras de :', len(CoeficienteActual1) #Se imprime el tamaño de la columna
		#print ' '
		z=str(len(CoeficienteActual1))
		archivo.write('\n\\textbf{Con un número de muestras de:} ' + z + '\n\n')
		#Barra grafico
		distancias.pop(0)# ELiminar coeficiente de energia
		Cepstros=np.arange(len(distancias))
		plt.bar(Cepstros, distancias)
		plt.title('Distancia Euclidea')
		plt.xlabel('Coeficientes Cepstrales')
		plt.ylabel('Distancia Euclidea')
		#print(' ')
		plt.savefig('../out/pictures/'+str(ind)+'.png')
		archivo.write('\n \\begin{figure}[H]\n \\centering\n \\includegraphics[width=1\\textwidth]{' + '../out/pictures/'+str(ind)+'.png' + '}\n')
		archivo.write('\\caption{Distancia euclídea: MFCC '+str(ind)+'}\n \\label{fig:'+str(ind)+'}\n \\end{figure}')
		plt.close()

def Mean_Absolute_Distance(): # calcula MAD y ecribe en informe completo
	#print(' ')
	#print('MAD:')
	rutacsv="../out/mfccAhocoder"
	rutacsvRuido="../out/mfccAhocoderRuido"
	archivoRuido = sorted(os.listdir(rutacsvRuido))
	ind=0
	archivo.write('\\newpage \n') #tex
	
	#Archivos de audio
	for archivoaudio in sorted(os.listdir(rutacsv)): #selecciona los archivos a comparar alfabeticamente
		rutaA=os.path.join(rutacsv,archivoaudio)
		archivoruidoRuta=archivoRuido[ind]
		rutaB=os.path.join(rutacsvRuido,archivoruidoRuta)
		ind=ind+1
		AlatexARCHI=archivoaudio.replace("_", "\\_")
		BlatexARCHI=archivoruidoRuta.replace("_", "\\_")
		csv=pd.read_csv(rutaA, sep='\t', header= None) #leer el archivo
		csvRuido=pd.read_csv(rutaB, sep='\t', header= None)
		#tex
		archivo.write('\\subsection{MAD para MFCC '+str(ind)+'}' + '\n\n')
		archivo.write('\\textbf{Los archivos analizados son:} \n\n')
		archivo.write('\\begin{enumerate} \n' + '\\item ' + AlatexARCHI + '\n')
		archivo.write('\\item ' + BlatexARCHI + '\n' + '\\end{enumerate} \n\n')

		#MAD
		ListaMAD=[]
		for n in range(0, 39):
			MFCCn=csv.iloc[:,n].values #se toma las columnas del archivo y se guardan como un array
			MFCCnRuido=csvRuido.iloc[:,n].values
			Abs=abs(MFCCn-MFCCnRuido)
			suma=0
			for i in Abs: #Para obtener la suma de los componentes de un vector
				suma=suma+i	
			suma=suma/len(Abs)# dividido entre el numero de muestras
			ListaMAD.append(suma)
		suma=0
		for j in ListaMAD:
			suma=suma+j
		MAD=suma/39
		#print "-"
		#print "MAD"
		#tex
		archivo.write('\\textbf{Los resultados para la métrica MAD son: } ' + '\n\n')
		archivo.write('\nMAD: ' + str(MAD) + '\n\n')
		archivo.write('\\textbf{La distancia media absoluta es:} ' + '\n\n ')

		#Inicio de tabla para latex
		archivo.write('\\begin{table}[H] \n')
		archivo.write('\\centering \n')
		archivo.write('\\caption{Distancia media absoluta para MFCC:'+str(ind)+'}\n')
		#archivo.write('\\label{tab:tab'+str(ind)+'}\n')
		archivo.write('\\begin{tabular}{|l|l|l|l|}\n')
		archivo.write('\\hline \n')
		archivo.write('MFCC & MAD & MFCC & MAD \\\\')
		archivo.write('\\hline \n')
		###########

		for conteo in range(len(ListaMAD)-19): #ciclo for para guardar la lista de distancias
			conteo2=str(conteo+1)
			conteolado2=str(conteo+21)
			y=str(ListaMAD[conteo])
			if conteo == 19:
				y2='-'
			else:
				y2=str(ListaMAD[conteo+20])
			archivo.write('MFCC ' + conteo2 + '&' + y + '&' 'MFCC ' + conteolado2 + '&' + y2)
			archivo.write('\\\\ \\hline\n')
		archivo.write('\\end{tabular} \n')
		archivo.write('\\end{table} \n')
		muestras=str(len(MFCCn))
		archivo.write('\n\\textbf{Número de muestras: } ' + muestras +'\n\n')
		archivo.write('\\newpage \n') #tex

		global array_ponderado_DAM
		global ponderado_DAM
		myarray = np.asarray(ListaMAD)#convierte lista en array
		array_ponderado_DAM=myarray+array_ponderado_DAM #Suma de arrays
		ponderado_DAM=MAD+ponderado_DAM #Suma MAD

	print 'MAD 100%	'
	
def distanceManhattan(): # calcula Manhattan y escribe en informe completo
	#print(' ')
	#print('Distance Manhattan:')
	
	rutacsv="../out/mfccAhocoder"
	rutacsvRuido="../out/mfccAhocoderRuido"
	archivoRuido = sorted(os.listdir(rutacsvRuido))
	ind=0
	archivo.write('\\newpage \n') #tex
	
	#Archivos de audio
	for archivoaudio in sorted(os.listdir(rutacsv)): #selecciona los archivos a comparar alfabeticamente
		rutaA=os.path.join(rutacsv,archivoaudio)
		archivoruidoRuta=archivoRuido[ind]
		rutaB=os.path.join(rutacsvRuido,archivoruidoRuta)
		ind=ind+1
		AlatexARCHI=archivoaudio.replace("_", "\\_")
		BlatexARCHI=archivoruidoRuta.replace("_", "\\_")
		csv=pd.read_csv(rutaA, sep='\t', header= None) #leer el archivo
		csvRuido=pd.read_csv(rutaB, sep='\t', header= None)
		#tex
		archivo.write('\\subsection{Distance Manhattan MFCC '+str(ind)+'}' + '\n\n')
		archivo.write('\\textbf{Los archivos analizados son:} \n\n')
		archivo.write('\\begin{enumerate} \n' + '\\item ' + AlatexARCHI + '\n')
		archivo.write('\\item ' + BlatexARCHI + '\n' + '\\end{enumerate} \n\n')

		#MAD
		ListaMAD=[]
		for n in range(0, 39):
			MFCCn=csv.iloc[:,n].values #se toma las columnas del archivo y se guardan como un array
			MFCCnRuido=csvRuido.iloc[:,n].values
			Abs=abs(MFCCn-MFCCnRuido)

			suma=0
			for i in Abs: #Para obtener la suma de los componentes de un vector
				suma=suma+i	
			ListaMAD.append(suma)
		#print "-"
		#print "Distance Manhattan"

		archivo.write('\\textbf{Los resultados para la métrica Distance Manhattan son: } ' + '\n\n')
		#Inicio de tabla para latex
		archivo.write('\\begin{table}[H] \n')
		archivo.write('\\centering \n')
		archivo.write('\\caption{Distance Manhattan para MFCC:'+str(ind)+'}\n')
		archivo.write('\\begin{tabular}{|l|l|l|l|}\n')
		archivo.write('\\hline \n')
		archivo.write('MFCC & Manhattan & MFCC & Manhattan \\\\')
		archivo.write('\\hline \n')
		###########

		for conteo in range(len(ListaMAD)-19): #ciclo for para guardar la lista de distancias
			conteo2=str(conteo+1)
			conteolado2=str(conteo+21)
			y=str(ListaMAD[conteo])
			if conteo == 19:
				y2='-'
			else:
				y2=str(ListaMAD[conteo+20])
			archivo.write('MFCC ' + conteo2 + '&' + y + '&' 'MFCC ' + conteolado2 + '&' + y2)
			archivo.write('\\\\ \\hline\n')
		archivo.write('\\end{tabular} \n')
		archivo.write('\\end{table} \n')
		muestras=str(len(MFCCn))
		archivo.write('\n\\textbf{Número de muestras: } ' + muestras +'\n\n')
		archivo.write('\\newpage \n') #tex

		global array_ponderado_DM
		myarray = np.asarray(ListaMAD)#convierte lista en array
		array_ponderado_DM=myarray+array_ponderado_DM #Suma de arrays


	print 'Distance Manhattan 100%	'


if __name__=='__main__':
	#Para archivo de latex informe total#
	archivo=open('../out/pdf/informe.tex', 'w')
	archivo.write('\\documentclass{article}\n\\usepackage[utf8]{inputenc}\n\\usepackage{graphicx}\n\\usepackage{float}\n\\title{Sistema de análisis espectral entre conjuntos de señales de habla: Métrica Distancia Euclídea, Distancia absoluta media (MAD) y Distancia de Manhattan}\n\\author{EIE}\n\\begin{document}\n\\maketitle\n')
	#arreglos para informe resumen
	array_ponderado_DE=zeros(40)
	ponderado_DAM=0.0
	array_ponderado_DAM=zeros(39)
	array_ponderado_DM=zeros(39)
	# banderas para informe resumen
	flagDE=0
	flagDAM=0
	flagDM=0

	def main(): 
		# interfaz#
		fieldnames = ['Distancia Euclideana', 'Distancia Absoluta Media', 'Distancia de Manhattan']
		cancelbotton=['None']
		choice = easygui.multchoicebox("Spectral Analysis", "", fieldnames)
		if choice is None:
			sys.exit()
		#
		title2='Seleccione la ruta con los audios sin ruido'
		title3='Seleccione la ruta con los audios con ruido'
		print(' ')
		ruta1=eg.diropenbox(title2) #Ruta para audios limpios
		ruta2=eg.diropenbox(title3) #Ruta para audios con ruido
		if ruta1 is None or ruta2 is None:
			main()
		# Si el directorio no contine .wav, o tiene cantidaddes diferentes de audio
		Nruta1=len(glob.glob(ruta1+"/*.wav"))
		Nruta2=len(glob.glob(ruta2+"/*.wav"))
		flag1=0
		flag2=0
		listdirectory = os.listdir(ruta1) # gets the name of all files in your dir
		for filename in listdirectory: 
			if not filename.endswith(".wav"): # check each of the files for whether or not they end in .wav
				flag1=1
		listdirectory = os.listdir(ruta2) # gets the name of all files in your dir
		for filename in listdirectory: 
			if not filename.endswith(".wav"): # check each of the files for whether or not they end in .wav
				flag2=1

		if Nruta1==Nruta2 and flag1==0 and flag2==0: # Si los directorios contienen cantidad archivos y son wav
			#Guardar la ruta1 en archivo txt
			file = open("../script/ruta/ruta1.txt", "w")
			file.write(ruta1)
			file.close()
			###

			#Guardar la ruta2 en archivo txt
			file = open("../script/ruta/ruta2.txt", "w")
			file.write(ruta2)
			file.close()
			###

			choices=['Ok','Cancelar']
			msg='Presione Ok para continuar y espere por favor!'
			msgEspera(choices,msg)

			os.system("sh ../script/resamplear_clean.sh") # llama script de resamplear_clean
			os.system("sh ../script/resamplear_ruido.sh") # llama script de cc_a_txt_Ruido
			print "Remuestreo 100%"
		
			os.system("sh ../script/ahocoder.sh") # llama script de ahocoder
			os.system("sh ../script/ahocoderRuido.sh") # llama script de ahocoderRuido
			print "Parametros en formato hexadecimal 100%"

			os.system("sh ../script/cc_a_txt.sh") # llama script de cc_a_txt
			os.system("sh ../script/cc_a_txt_Ruido.sh") # llama script de cc_a_txt_Ruido
			print "Parametros en formato decimal 100%"
			
			#Datos en terminal
			print(' ')
			print('Ruta de audios: ' + ruta1)
			print('Ruta de audios con ruido: ' + ruta2)
			print(' ')
			

			if fieldnames[0] in choice: #euclidea
				archivo.write('\\newpage \n')
				archivo.write('\\section{Distancia Euclídea} \n')
				global flagDE
				flagDE=1
				distanceEuclidean()
				print('Distance Euclide 100%')

			if fieldnames[1] in choice: #MAD
				archivo.write('\\newpage \n')
				archivo.write('\\section{MAD} \n')
				thread2=threading.Thread(target=Mean_Absolute_Distance)
				thread2.start()
				thread2.join()
				global flagDAM
				flagDAM=1

			if fieldnames[2] in choice: #Manhattan
				archivo.write('\\newpage \n')
				archivo.write('\\section{Distancia de Manhattan} \n')
				thread3=threading.Thread(target=distanceManhattan)
				thread3.start()
				thread3.join()
				global flagDM
				flagDM=1

			archivo.write('\n\\end{document}') #cierra el documento
			archivo.close()

			resumen(Nruta1,flagDE,flagDAM,flagDM)#Crea resumen de los resultados
			print(' ')
			print(' ')
			os.system("sh ../script/latex.sh") # llama script de ltex.sh
			
			choices=['Salir','Ver Resultados Completo','Ver Resultados Resumen']
			msg='Análisis finalizado'

			#Eliminar contenido de directorios
			vaciarDir("../out/resa_clean",".wav")
			vaciarDir("../out/resa_ruido",".wav")
			vaciarDir("../out/mfccAhocoder",".csv")
			vaciarDir("../out/mfccAhocoderRuido",".csv")
			vaciarDir("../out/mfcchexadecimal",".cc")
			vaciarDir("../out/mfcchexadecimalRuido",".cc")
			vaciarDir("../out/pictures",".png")

			msgFin(choices,msg)

		else: #Si los directorios no son correctos
			title='Análisis espectral'
			msg='Error! Cantidad de archivos no coinciden o no son archivos .wav'
			choices=['Volver al menú','Salir']
			eleccion=easygui.buttonbox(msg, title, choices)
			if eleccion == choices[0]:
				main()
			else:
				sys.exit()

	main()